#!/usr/bin/env python2
"""Connects to and captures data from Shimmer3 wireless body sensors."""

#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import rospy
import sensor_msgs.msg
import serial
import struct

from shimmer3.msg import EMG
from shimmer3.msg import ExG


def wait_for_ack(ser):
    """Wait for acknowledgement bit from device.

    Keyword arguments:
    ser -- serial port
    """
    ddata = ""
    ack = struct.pack('B', 0xff)
    while ddata != ack:
        ddata = ser.read(1)
        print "received [%s]" % struct.unpack('B', ddata)
        return


def emg_calc(data):
    """Calculate mV values of EMG from raw output.

    Keyword arguments:
    data -- raw data from device
    """
    value = float(data) * float(0.00002404054)
    return value


def accel_calc(data):
    """Calculate m/s^2 values of accelerometer from raw output.

    Keyword arguments:
    data -- raw data from device
    """
    value = data * float(1) * float(0.00006103515) * float(9.80665)
    return value


def gyro_calc(data):
    """Calculate deg/s values of gyroscope from raw output.

    Keyword arguments:
    data -- raw data from device
    """
    value = data / float(65.5)
    return value


def mag_calc_xy(data):
    """Calculate gauss values of magnometer from raw output, X and Y.

    Keyword arguments:
    data -- raw data from device
    """
    value = data * float(0.00149253731)
    return value


def mag_calc_z(data):
    """Calculate gauss values of magnometer from raw output, Z.

    Keyword arguments:
    data -- raw data from device
    """
    value = data * float(0.00166666666)
    return value


if __name__ == '__main__':
    rospy.init_node('ACC')

    imu_raw_pub = rospy.Publisher('raw/imu', sensor_msgs.msg.Imu, queue_size=1)
    imu_raw = sensor_msgs.msg.Imu()
    imu_raw.header.frame_id = "imu"
    imu_raw.orientation.x = 0.0
    imu_raw.orientation.y = 0.0
    imu_raw.orientation.z = 0.0
    imu_raw.orientation.w = 0.0
    imu_raw.orientation_covariance = [0.0] * 9
    imu_raw.orientation_covariance[0] = -1.0  # covariance unknown
    imu_raw.angular_velocity_covariance = [0.0] * 9
    imu_raw.angular_velocity_covariance[0] = -1.0  # covariance unknown
    imu_raw.linear_acceleration_covariance = [0.0] * 9
    imu_raw.linear_acceleration_covariance[0] = -1.0  # covariance unknown

    imu_pub = rospy.Publisher(
        'calculated/imu', sensor_msgs.msg.Imu, queue_size=1)
    imu = sensor_msgs.msg.Imu()
    imu.header.frame_id = "imu"
    imu.orientation.x = 0.0
    imu.orientation.y = 0.0
    imu.orientation.z = 0.0
    imu.orientation.w = 0.0
    imu.orientation_covariance = [0.0] * 9
    imu.orientation_covariance[0] = -1.0  # covariance unknown
    imu.angular_velocity_covariance = [0.0] * 9
    imu.angular_velocity_covariance[0] = -1.0  # covariance unknown
    imu.linear_acceleration_covariance = [0.0] * 9
    imu.linear_acceleration_covariance[0] = -1.0  # covariance unknown

    mag_raw_pub = rospy.Publisher(
        'raw/mag', sensor_msgs.msg.MagneticField, queue_size=1)
    mag_raw = sensor_msgs.msg.MagneticField()
    mag_raw.header.frame_id = "imu"
    mag_raw.magnetic_field_covariance = [0.0] * 9
    mag_raw.magnetic_field_covariance[0] = -1.0  # covariance unknown

    mag_pub = rospy.Publisher(
        'calculated/mag', sensor_msgs.msg.MagneticField, queue_size=1)
    mag = sensor_msgs.msg.MagneticField()
    mag.header.frame_id = "imu"
    mag.magnetic_field_covariance = [0.0] * 9
    mag.magnetic_field_covariance[0] = -1.0  # covariance unknown

    exg_pub = rospy.Publisher('raw/exg', ExG, queue_size=1)
    exg = ExG()
    exg.header.frame_id = "imu"
    exg.c1status = 0.0
    exg.c1ch1 = 0.0
    exg.c1ch2 = 0.0
    exg.c2status = 0.0
    exg.c2ch1 = 0.0
    exg.c2ch2 = 0.0

    emg_pub = rospy.Publisher('calculated/emg', EMG, queue_size=1)
    emg = EMG()
    emg.header.frame_id = "imu"
    emg.emg_c1ch1 = 0.0
    emg.emg_c1ch2 = 0.0

    ser = serial.Serial("/dev/rfcomm0", 115200)
    ser.flushInput()

    # send the set sensors command
    # Accel,Gyro,Mag,EMG24bit
    ser.write(struct.pack('BBBB', 0x08, 0x78, 0x10, 0x00))
    wait_for_ack(ser)
    # send the set sampling rate command
    # set sampling rate (512Hz)
    ser.write(struct.pack('BBB', 0x05, 0x40, 0x00))
    wait_for_ack(ser)
    # set accel
    # set sampling rate (400Hz)
    ser.write(struct.pack('BB', 0x40, 0x07))
    wait_for_ack(ser)
    # set range (2g)
    ser.write(struct.pack('BB', 0x09, 0x00))
    wait_for_ack(ser)
    # set low power mode (off)
    ser.write(struct.pack('BB', 0x43, 0x00))
    wait_for_ack(ser)
    # set high resolution mode (off)
    ser.write(struct.pack('BB', 0x46, 0x00))
    wait_for_ack(ser)
    # set gyro
    # set range (500dps)
    ser.write(struct.pack('BB', 0x49, 0x01))
    wait_for_ack(ser)
    # set mag
    # set gain (5g)
    ser.write(struct.pack('BB', 0x37, 0x03))
    wait_for_ack(ser)
    # set sampling rate (220Hz)
    ser.write(struct.pack('BB', 0x3A, 0x07))
    wait_for_ack(ser)
    # set EMG chips
    ser.write(struct.pack('BBBBBBBBBBBBBB',
                          0x61, 0x00, 0x00, 0x0A, 0x02, 0xA0, 0x10, 0x69, 0x60,
                          0x20, 0x00, 0x00, 0x02, 0x03))
    wait_for_ack(ser)
    ser.write(struct.pack('BBBBBBBBBBBBBB',
                          0x61, 0x01, 0x00, 0x0A, 0x02, 0xA0, 0x10, 0x61, 0x61,
                          0x00, 0x00, 0x00, 0x02, 0x01))
    wait_for_ack(ser)
    # send start streaming command
    ser.write(struct.pack('B', 0x07))
    wait_for_ack(ser)

    ddata = ""
    numbytes = 0
    # Packet type (1), TimeStamp (2),
    # 3xAccel (3x2), 3xGyro (3x2), 3xMag (3x2), 2xEMG(2x7)
    framesize = 35

    while not rospy.is_shutdown():
        try:
            while numbytes < framesize:
                ddata += ser.read(framesize)
                numbytes = len(ddata)
                data = ddata[0:framesize]
            ddata = ddata[framesize:]
            numbytes = len(ddata)
            (packettype) = struct.unpack('B', data[0])
            (timestamp) = struct.unpack('H', data[1:3])
            (imu_raw.angular_velocity.x, imu_raw.angular_velocity.y,
             imu_raw.angular_velocity.z) = struct.unpack('>hhh', data[3:9])
            (imu_raw.linear_acceleration.x, imu_raw.linear_acceleration.y,
             imu_raw.linear_acceleration.z) = struct.unpack('hhh', data[9:15])
            (mag.magnetic_field.x, mag.magnetic_field.z,
             mag.magnetic_field.y) = struct.unpack('>hhh', data[15:21])
            (exg.c1status,) = struct.unpack('B', data[21])
            exg.c1ch1 = struct.unpack('>i', (data[22:25] + '\0'))[0] >> 8
            exg.c1ch2 = struct.unpack('>i', (data[25:28] + '\0'))[0] >> 8
            (exg.c2status,) = struct.unpack('B', data[28])
            exg.c2ch1 = struct.unpack('>i', (data[29:32] + '\0'))[0] >> 8
            exg.c2ch2 = struct.unpack('>i', (data[32:35] + '\0'))[0] >> 8
            imu.linear_acceleration.x = accel_calc(
                imu_raw.linear_acceleration.x
                )
            imu.linear_acceleration.y = accel_calc(
                imu_raw.linear_acceleration.y
                )
            imu.linear_acceleration.z = accel_calc(
                imu_raw.linear_acceleration.z
                )
            imu.angular_velocity.x = gyro_calc(imu_raw.angular_velocity.x)
            imu.angular_velocity.y = gyro_calc(imu_raw.angular_velocity.y)
            imu.angular_velocity.z = gyro_calc(imu_raw.angular_velocity.z)
            mag.magnetic_field.x = mag_calc_xy(mag_raw.magnetic_field.x)
            mag.magnetic_field.y = mag_calc_xy(mag_raw.magnetic_field.y)
            mag.magnetic_field.z = mag_calc_z(mag_raw.magnetic_field.z)
            emg.emg_c1ch1 = emg_calc(exg.c1ch1)
            emg.emg_c1ch2 = emg_calc(exg.c1ch2)
        except serial.SerialException as e:
            rospy.logerr(
                "SerialException during read_data: {0}".format(e.strerror))
        time = rospy.Time.now()
        imu_raw.header.stamp = time
        imu_raw_pub.publish(imu_raw)
        mag_raw.header.stamp = time
        mag_raw_pub.publish(mag_raw)
        exg.header.stamp = time
        exg_pub.publish(exg)
        imu.header.stamp = time
        imu_pub.publish(imu)
        mag.header.stamp = time
        mag_pub.publish(mag)
        emg.header.stamp = time
        emg_pub.publish(emg)
        rospy.logdebug("published!")
        rospy.sleep(0.002)

    # send stop streaming command
    ser.write(struct.pack('B', 0x20))
    wait_for_ack(ser)
    # close the socket
    ser.close()
    print "\n"
