#!/usr/bin/env python2
"""Connects to and captures data from Shimmer3 wireless body sensors."""

#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import rospy
import sensor_msgs.msg
import serial
import struct

from shimmer3.msg import GSR


def wait_for_ack(ser):
    """Wait for acknowledgement bit from device.

    Keyword arguments:
    ser -- serial port
    """
    ddata = ""
    ack = struct.pack('B', 0xff)
    while ddata != ack:
        ddata = ser.read(1)
        print "received [%s]" % struct.unpack('B', ddata)
        return


def gsr_calc(data):
    """Calculate gsr value.

    Keyword arguments:
    data -- raw data from device
    """
    if (gsr.gsrrange == 0):
        p1 = 0.0363
        p2 = -24.8617
    if (gsr.gsrrange == 1):
        p1 = 0.0051
        p2 = -3.8357
    if (gsr.gsrrange == 2):
        p1 = 0.0015
        p2 = -1.0067
    if (gsr.gsrrange == 3):
        p1 = 4.4513e-04
        p2 = -0.3193
    value = (float(p1) * data) + float(p2)
    return value


def ppg_calc(data):
    """PLACEHOLDER."""
    value = data * float(0.00024414062)
    return value


def accel_calc(data):
    """Calculate m/s^2 values of accelerometer from raw output.

    Keyword arguments:
    data -- raw data from device
    """
    value = data * float(1) * float(0.00006103515) * float(9.80665)
    return value


def gyro_calc(data):
    """Calculate deg/s values of gyroscope from raw output.

    Keyword arguments:
    data -- raw data from device
    """
    value = data / float(65.5)
    return value


def mag_calc_xy(data):
    """Calculate gauss values of magnometer from raw output, X and Y.

    Keyword arguments:
    data -- raw data from device
    """
    value = data * float(0.00149253731)
    return value


def mag_calc_z(data):
    """Calculate gauss values of magnometer from raw output, Z.

    Keyword arguments:
    data -- raw data from device
    """
    value = data * float(0.00166666666)
    return value


if __name__ == '__main__':
    rospy.init_node('GSR')

    imu_raw_pub = rospy.Publisher('raw/imu', sensor_msgs.msg.Imu, queue_size=1)
    imu_raw = sensor_msgs.msg.Imu()
    imu_raw.header.frame_id = "imu"
    imu_raw.orientation.x = 0.0
    imu_raw.orientation.y = 0.0
    imu_raw.orientation.z = 0.0
    imu_raw.orientation.w = 0.0
    imu_raw.orientation_covariance = [0.0] * 9
    imu_raw.orientation_covariance[0] = -1.0  # covariance unknown
    imu_raw.angular_velocity_covariance = [0.0] * 9
    imu_raw.angular_velocity_covariance[0] = -1.0  # covariance unknown
    imu_raw.linear_acceleration_covariance = [0.0] * 9
    imu_raw.linear_acceleration_covariance[0] = -1.0  # covariance unknown

    imu_pub = rospy.Publisher(
        'calculated/imu', sensor_msgs.msg.Imu, queue_size=1)
    imu = sensor_msgs.msg.Imu()
    imu.header.frame_id = "imu"
    imu.orientation.x = 0.0
    imu.orientation.y = 0.0
    imu.orientation.z = 0.0
    imu.orientation.w = 0.0
    imu.orientation_covariance = [0.0] * 9
    imu.orientation_covariance[0] = -1.0  # covariance unknown
    imu.angular_velocity_covariance = [0.0] * 9
    imu.angular_velocity_covariance[0] = -1.0  # covariance unknown
    imu.linear_acceleration_covariance = [0.0] * 9
    imu.linear_acceleration_covariance[0] = -1.0  # covariance unknown

    mag_raw_pub = rospy.Publisher(
        'raw/mag', sensor_msgs.msg.MagneticField, queue_size=1)
    mag_raw = sensor_msgs.msg.MagneticField()
    mag_raw.header.frame_id = "imu"
    mag_raw.magnetic_field_covariance = [0.0] * 9
    mag_raw.magnetic_field_covariance[0] = -1.0  # covariance unknown

    mag_pub = rospy.Publisher(
        'calculated/mag', sensor_msgs.msg.MagneticField, queue_size=1)
    mag = sensor_msgs.msg.MagneticField()
    mag.header.frame_id = "imu"
    mag.magnetic_field_covariance = [0.0] * 9
    mag.magnetic_field_covariance[0] = -1.0  # covariance unknown

    gsr_raw_pub = rospy.Publisher('raw/gsr', GSR, queue_size=1)
    gsr_raw = GSR()
    gsr_raw.header.frame_id = "imu"
    gsr_raw.gsr = 0.0
    gsr_raw.gsrrange = 0.0
    gsr_raw.adc12 = 0.0
    gsr_raw.adc13 = 0.0

    gsr_pub = rospy.Publisher('calculated/gsr', GSR, queue_size=1)
    gsr = GSR()
    gsr.header.frame_id = "imu"
    gsr.gsr = 0.0
    gsr.gsrrange = 0.0
    gsr.adc12 = 0.0
    gsr.adc13 = 0.0

    ser = serial.Serial("/dev/rfcomm3", 115200)
    ser.flushInput()

    # send the set sensors command
    # Accel,Gyro,Mag,GSR,ADC12,ADC13
    ser.write(struct.pack('BBBB', 0x08, 0x64, 0x13, 0x00))
    wait_for_ack(ser)
    # enable expansion power
    ser.write(struct.pack('BB', 0x5E, 0x01))
    wait_for_ack(ser)
    # send the set sampling rate command
    # set sampling rate (512Hz)
    ser.write(struct.pack('BBB', 0x05, 0x40, 0x00))
    wait_for_ack(ser)
    # set accel
    # set sampling rate (400Hz)
    ser.write(struct.pack('BB', 0x40, 0x07))
    wait_for_ack(ser)
    # set range (2g)
    ser.write(struct.pack('BB', 0x09, 0x00))
    wait_for_ack(ser)
    # set low power mode (off)
    ser.write(struct.pack('BB', 0x43, 0x00))
    wait_for_ack(ser)
    # set high resolution mode (off)
    ser.write(struct.pack('BB', 0x46, 0x00))
    wait_for_ack(ser)
    # set gyro
    # set range (500dps)
    ser.write(struct.pack('BB', 0x49, 0x01))
    wait_for_ack(ser)
    # set mag
    # set gain (5g)
    ser.write(struct.pack('BB', 0x37, 0x03))
    wait_for_ack(ser)
    # set sampling rate (220Hz)
    ser.write(struct.pack('BB', 0x3A, 0x07))
    wait_for_ack(ser)
    # set gsr
    # ser.write(struct.pack('BB', 0x21, 0x04))
    # send start streaming command
    ser.write(struct.pack('B', 0x07))
    wait_for_ack(ser)

    ddata = ""
    numbytes = 0
    # Packet type (1), TimeStamp (2),
    # 3xAccel (3x2), 3xGyro (3x2), 3xMag (3x2), 2xEMG(2x7)
    framesize = 27

    while not rospy.is_shutdown():
        try:
            while numbytes < framesize:
                ddata += ser.read(framesize)
                numbytes = len(ddata)
            data = ddata[0:framesize]
            ddata = ddata[framesize:]
            numbytes = len(ddata)

            (packettype,) = struct.unpack('B', data[0:1])
            (timestamp,
             gsr_raw.adc12,
             gsr_raw.adc13,
             gsr_raw.gsr) = struct.unpack('HHHH', data[1:9])
            gsr_raw.gsrrange = (gsr_raw.gsr & 0xC000) >> 14
            gsr_raw.gsr &= 0xFFF
            (imu_raw.angular_velocity.x,
             imu_raw.angular_velocity.y,
             imu_raw.angular_velocity.z) = struct.unpack('>hhh', data[9:15])
            (imu_raw.linear_acceleration.x,
             imu_raw.linear_acceleration.y,
             imu_raw.linear_acceleration.z) = struct.unpack('hhh', data[15:21])
            (mag.magnetic_field.x,
             mag.magnetic_field.z,
             mag.magnetic_field.y) = struct.unpack('>hhh', data[21:27])
            gsr.gsrrange = gsr_raw.gsrrange
            gsr.gsr = gsr_calc(gsr_raw.gsr)
            gsr.adc13 = ppg_calc(gsr_raw.adc13)
            imu.linear_acceleration.x = accel_calc(
                imu_raw.linear_acceleration.x)
            imu.linear_acceleration.y = accel_calc(
                imu_raw.linear_acceleration.y)
            imu.linear_acceleration.z = accel_calc(
                imu_raw.linear_acceleration.z)
            imu.angular_velocity.x = gyro_calc(imu_raw.angular_velocity.x)
            imu.angular_velocity.y = gyro_calc(imu_raw.angular_velocity.y)
            imu.angular_velocity.z = gyro_calc(imu_raw.angular_velocity.z)
            mag.magnetic_field.x = mag_calc_xy(mag_raw.magnetic_field.x)
            mag.magnetic_field.y = mag_calc_xy(mag_raw.magnetic_field.y)
            mag.magnetic_field.z = mag_calc_z(mag_raw.magnetic_field.z)
        except serial.SerialException as e:
            rospy.logerr(
                "SerialException during read_data: {0}".format(e.strerror))
        time = rospy.Time.now()
        imu_raw.header.stamp = time
        imu_raw_pub.publish(imu_raw)
        mag_raw.header.stamp = time
        mag_raw_pub.publish(mag_raw)
        gsr_raw.header.stamp = time
        gsr_raw_pub.publish(gsr_raw)
        imu.header.stamp = time
        imu_pub.publish(imu)
        mag.header.stamp = time
        mag_pub.publish(mag)
        gsr.header.stamp = time
        gsr_pub.publish(gsr)
        rospy.logdebug("published!")
        rospy.sleep(0.002)

    # send stop streaming command
    ser.write(struct.pack('B', 0x20))
    wait_for_ack(ser)
    # close the socket
    ser.close()
    print "\n"
