#!/bin/bash

clear

if [ "$(whoami)" != "root" ]
    then
        echo "Must run as root."
        echo "Exiting..."
        exit 1
fi

echo "This script will unbind all devices from rfcomm 0 through 3 and then "
echo "bind the Shimmer3 body sensors to these ports."
echo " "
read -p "Press any key to continue" -n1 -s

echo "Releasing rfcomm 0 through 3..."
rfcomm release 0 &>/dev/null
rfcomm release 1 &>/dev/null
rfcomm release 2 &>/dev/null
rfcomm release 3 &>/dev/null
echo "Done."

echo "Binding ACC (8612) to rfcomm0..."
rfcomm bind 0 00:06:66:66:86:12
echo "Done."

echo "Binding ECG (948A) to rfcomm1..."
rfcomm bind 1 00:06:66:66:94:8A
echo "Done."

echo "Binding EMG (9585) to rfcomm2..."
rfcomm bind 2 00:06:66:66:95:85
echo "Done."

echo "Binding GSR (955D) to rfcomm3..."
rfcomm bind 3 00:06:66:66:95:5D
echo "Done."

echo "Shimmers bound to rfcomm channels."
echo "Exiting..."
