Shimmer3 ROS Package
====================

[![Build Status](https://drone.io/bitbucket.org/zkoesters/shimmer3/status.png)](https://drone.io/bitbucket.org/zkoesters/shimmer3/latest)

ROS package for Shimmer3 wireless body sensors.
